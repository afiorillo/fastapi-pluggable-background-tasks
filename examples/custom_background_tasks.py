import time
from pathlib import Path
from uuid import uuid1

from fastapi import FastAPI, Depends
from fastapi.responses import PlainTextResponse

from fastapi_background_tasks import DurableBackgroundTasks

class Counter:
    def __init__(self, start=0):
        self.value = start

    def inc(self, by_x=1):
        self.value += by_x

counter = Counter()
app = FastAPI()
tasks_dir = Path('tasks')
tasks_dir.mkdir(parents=True, exist_ok=True)

async def process_hit(hit_num: int, enqueue_time: float):
    print(f'processed {hit_num=} queued for {time.monotonic()-enqueue_time:.2f} seconds')
    time.sleep(1) # synchronous!

@app.get('/')
async def monitored_hello(counter: Counter = Depends(lambda : counter)):
    request_id = uuid1().hex
    background_tasks = DurableBackgroundTasks(tasks_dir, prefix=request_id + '_')
    counter.inc()
    background_tasks.add_task(process_hit, counter.value, time.monotonic())
    return PlainTextResponse(f'hit {counter.value}', background=background_tasks)
