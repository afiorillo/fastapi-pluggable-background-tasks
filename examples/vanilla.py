import time

from fastapi import FastAPI, BackgroundTasks, Depends


class Counter:
    def __init__(self, start=0):
        self.value = start

    def inc(self, by_x=1):
        self.value += by_x

counter = Counter()
app = FastAPI()

def process_hit(hit_num: int, enqueue_time: float):
    print(f'processed {hit_num=} queued for {time.monotonic()-enqueue_time:.2f} seconds')
    time.sleep(1) # synchronous!

@app.get('/')
async def monitored_hello(background_tasks: BackgroundTasks, counter: Counter = Depends(lambda : counter)):
    counter.inc()
    background_tasks.add_task(process_hit, counter.value, time.monotonic())
    return f'hit {counter.value}'
