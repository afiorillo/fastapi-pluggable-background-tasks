# Examples

Each of these examples is meant to be run in two steps.

First, you must start the server in the respective example.
For example:

```bash
$ uvicorn examples.vanilla:app
INFO:     Started server process [14696]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

Then, you should use your favorite load testing tool to send some requests to the example.
Below, I will use [hey](https://github.com/rakyll/hey).

```bash
# no concurrency, 10 requests
$ hey -c 1 -n 10 http://127.0.0.1:8000
# defaults, 50 threads, 200 requests
$ hey http://127.0.0.1:8000
```

By monitoring the output of the `uvicorn` process, we can follow the behavior of the example.

## Vanilla

**Run with:**

```bash
$ uvicorn examples.vanilla:app
```

This is meant to mimic the [documented usage](https://fastapi.tiangolo.com/tutorial/background-tasks/) of vanilla Background Tasks.
This example:

1. Has some global state to help us track the relative progress of handling requests vs. handling the background tasks.
2. Has some relatively slow function to handle the tasks.
3. Prints the delay between request time and task-completion time to stdout.

In cases of high load we might expect the backlog of tasks to grow faster than they are cleared.
In such a situation, the task backlog is not tolerant to server failure -- if the process exits all of the incomplete tasks are permanently lost.

Situations like this are why the FastAPI docs [recommend](https://fastapi.tiangolo.com/tutorial/background-tasks/#caveat) considering a more suitable queuing solution.
An alternative approach would be to enable users to plug in a more durable `BackgroundTasks`.

Note that since the background task involves *synchronously* working (i.e. `time.sleep`), this dramatically impacts latency of the endpoints.

## Vanilla (async)

**Run with:**

```bash
$ uvicorn examples.vanilla_async:app
```

This is a less contrived example than `vanilla.py` and shows that in reasonable circumstances, the processing backlog will not grow much.
In particular, you notice in the server output that each request line is logged almost exactly after handling the request, and the queue time is almost exactly 0 seconds.
This is because the server is able to handle other requests while the background task is `await asyncio.sleep(...)`ing, but the time between handling the request and invoking the task is almost nil.

Still! In the case where the background task involves some sort of atomic operation, a failure while executing this task could result in the data being lost.

## Custom Background Tasks

**Run with:**

```bash
$ uvicorn examples.custom_background_tasks:app
```

This example uses the `DurableBackgroundTasks` implementation in the package.
In particular, this example shows how to use any callable object (ours just happens to look like `starlette.background.BackgroundTasks`) for background tasks.
This one persists each task to disk between scheduling time (during handling the request) and execution time (after response is served).
It does this with `aiofiles`, though even then the performance is pretty bad.

A more realistic example might use an asynchronous client to Redis, with a queue named for each `request_id`, and pushing to that queue as each task is scheduled.
Of course, in that case, you would have to worry about *Redis'* durability, which is often expected to lose up to 60 seconds of data (at the expense of high performance).
