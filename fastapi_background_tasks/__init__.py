import aiofiles
from hashlib import sha256
from pathlib import Path
import pickle
from time import monotonic

import sys
import typing

if sys.version_info >= (3, 10):  # pragma: no cover
    from typing import ParamSpec
else:  # pragma: no cover
    from typing_extensions import ParamSpec

from starlette.background import BackgroundTask

P = ParamSpec("P")

class DurableBackgroundTasks(BackgroundTask):
    """
    This is a simple re-implementation of `starlette.background.BackgroundTasks` which
    persists each task as a file on the file system (pickled!) before later reading and
    executing that task.

    A more sophisticated example might serialize the task and send it to a Redis, as an
    example, though this serves as an example of any custom (and perhaps poor)
    implementation.
    """
    def __init__(
        self,
        storage_dir: Path,
        tasks: typing.Optional[typing.Sequence[BackgroundTask]] = None,
        prefix: str = '',
    ):
        self.dir = storage_dir.resolve()
        self.prefix = prefix
        self.tasks = list(tasks) if tasks else []

    @staticmethod
    def pure_task_name(task: BackgroundTask) -> str:
        return sha256(
            (task.func.__name__ + str(task.args) + str(task.kwargs)).encode()
        ).hexdigest()

    def add_task(
        self, func: typing.Callable[P, typing.Any], *args: P.args, **kwargs: P.kwargs
    ) -> None:
        task = BackgroundTask(func, *args, **kwargs)
        fp = self.dir.joinpath(f'{self.prefix}{monotonic()}_{self.pure_task_name(task)}.pickle')
        with fp.open('wb') as fOut:
            pickle.dump(task, fOut)

    async def __call__(self) -> None:
        for fp in sorted(list(self.dir.glob(f'{self.prefix}*.pickle'))):
            async with aiofiles.open(fp, mode='rb') as fIn:
                buf = await fIn.read()
                task = pickle.loads(buf)
                await task()
            fp.unlink()  # remove it from the queue, only if the task finished!
